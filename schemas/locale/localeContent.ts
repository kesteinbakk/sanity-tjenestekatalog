//import createSchema from 'part:@sanity/base/schema-creator'
//import schemaTypes from 'all:part:@sanity/base/schema-type'
import {/*  getDefLangFunc, */ getAllLangsFunc } from '../../language/lang';
import { getSortedLangs } from '../../settings';
import { firstUpper } from '../../utils/utils';

//const lang = getDefLangFunc('localeContent');
type LocalContentNames = 'content' | 'customerContent' | 'serviceContent' | 'serviceLevelContent';

const supportedLanguages = getSortedLangs();

const langs = getAllLangsFunc('localeContent');

type GetLocalContentObjProps = {
  name?: string,
  title?: string | boolean,
  body?: string | boolean,
}


const getLocaleContent = ({
  name = 'content',
  title = 'title',
  body = 'content'
}: GetLocalContentObjProps) => supportedLanguages.map(id => {


  const fields = [
    ...title ? [{
      name: title,
      title: langs('fields', title)[id],
      type: 'string',
      validation: Rule => Rule.required(),
    }] : [],
    ...body ? [{
      name: body,
      title: langs('fields', body)[id],
      type: 'blockContent'
    }] : []
  ]

  // console.debug("Fields are ", fields);

  return ({
    name: id + firstUpper(name),
    title: langs('languages', id)[id],
    type: 'object',
    fields,
  }
  );
});




export const getLocaleContentObj = (typeName: LocalContentNames = 'content') => supportedLanguages.map(id => {
  const name = id + firstUpper(typeName);
  return ({
    name,
    title: langs('languages', id)[id],
    type: name,
  });
});

// For schema definitions of the fields getLocalContent function may generate
export const localeContent = getLocaleContent({});

export const serviceLevelContent = getLocaleContent({
  name: 'serviceLevelContent',
  title: 'title',
  body: 'explanation'
});


export const customerContent = getLocaleContent({
  name: 'customerContent',
  title: false,
  body: 'notes'
});