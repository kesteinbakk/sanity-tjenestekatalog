//import createSchema from 'part:@sanity/base/schema-creator'
//import schemaTypes from 'all:part:@sanity/base/schema-type'


import { getLangFunc, getAllLangsFunc } from '../../language/lang';
import { supportedLanguages, defaultLang } from '../../settings';

const lang = getLangFunc('localeString');
const langs = getAllLangsFunc('localeString');


export const localeString = {
  name: 'localeString',
  title: lang('title'),
  type: 'object',
  // Fieldsets can be used to group object fields.
  // Here we omit a fieldset for the "default language",
  // making it stand out as the main field.
  fieldsets: [
    {
      name: 'translations',
      title: lang('translations'),
      options: {
        collapsible: true,
        collapsed: false,
        // columns: 2
      }
    }
  ],
  // Dynamically define one field per language
  fields: supportedLanguages.map(id => ({
    name: id,
    title: langs('languages', id)[id],
    type: 'string',
    fieldset: id === defaultLang ? null : 'translations'
  }))
};