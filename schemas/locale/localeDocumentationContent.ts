//import createSchema from 'part:@sanity/base/schema-creator'
//import schemaTypes from 'all:part:@sanity/base/schema-type'
import { getAllLangsFunc } from '../../language/lang';
import { getSortedLangs, Lang } from '../../settings';

//const lang = getDefLangFunc('localeServiceContent');
const langs = getAllLangsFunc('documentation');

const sortedLangs = getSortedLangs();

type ContentField = {
  name: string,
  title: string,
  type: string,
  to?: { [to: string]: string }[],
  options?: { [key: string]: any },
}

type Content = {
  name: string,
  langId: Lang,
  title: string,
  type: string,
  fields: ContentField[]
}

const localeDocumentationContent: Content[] = sortedLangs.map((id) => {
  return ({
    name: `${id}DocumentationContent`,
    langId: id,
    title: `${langs('content')[id]} (${id})`,
    type: 'object',
    /*   options: {
        columns: supportedLanguages.length <= 3 ? supportedLanguages.length : 3
      }, */
    fields: [
      {
        name: 'title',
        title: langs('fields', 'title')[id],
        type: 'string',
        validation: Rule => Rule.required().max(55),
      },
      {
        name: 'subtitle',
        title: langs('fields', 'subtitle')[id],
        type: 'string',
        validation: Rule => Rule.max(75)
      },

      {
        name: 'content',
        title: langs('fields', 'content')[id],
        type: 'blockContent',
        validation: Rule => Rule.required(),
      },

    ]
  });
});

export default localeDocumentationContent;