import { getAllLangsFunc } from "../../language/lang";
import { getSortedLangs, Lang } from "../../settings";

type ServiceContentField = {
  name: string,
  title: string,
  type: string,
  to?: { [to: string]: string }[],
  options?: { [key: string]: any },
}

type ServiceContent = {
  name: string,
  langId: Lang,
  title: string,
  type: string,
  fields: ServiceContentField[]
}

const sortedLangs = getSortedLangs();
const langs = getAllLangsFunc('services');

const localeServiceContent: ServiceContent[] = sortedLangs.map((id) => {
  return ({
    name: `${id}ServiceContent`,
    langId: id,
    title: `${langs('content')[id]} (${id})`,
    type: 'object',
    /*   options: {
        columns: supportedLanguages.length <= 3 ? supportedLanguages.length : 3
      }, */
    fields: [
      {
        name: 'title',
        title: langs('fields', 'title')[id],
        type: 'string',
        validation: Rule => Rule.required().max(55),
      },
      {
        name: 'subtitle',
        title: langs('fields', 'subtitle')[id],
        type: 'string',
        validation: Rule => Rule.max(75)
      },
      /*       {
              name: 'lead',
              title: langs('fields', 'lead')[id],
              type: 'text',
              rows: 2,
              validation: Rule => Rule.max(110)
            }, */
      {
        name: 'serviceDescription',
        title: langs('fields', 'serviceDescription')[id],
        type: 'blockContent',
        validation: Rule => Rule.required(),
      },
      {
        name: 'delivery',
        title: langs('fields', 'delivery')[id],
        type: 'blockContent',
      },
      {
        name: 'serviceLevel',
        title: langs('fields', 'serviceLevel')[id],
        type: 'reference',
        to: [{ type: 'serviceLevel' }],
        options: {
          disableNew: false
        }
      },
      {
        name: 'invoicing',
        title: langs('fields', 'invoicing')[id],
        type: 'string'
      },
      {
        name: 'ordering',
        title: langs('fields', 'ordering')[id],
        type: 'string'
      },
    ]
  });
});

export default localeServiceContent;