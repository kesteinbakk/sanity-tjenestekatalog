import { getLangFunc } from "../../language/lang";
import { getLocaleContentObj } from "../locale/localeContent";

const lang = getLangFunc('generalContent');

const generalContent = {
  name: 'generalContent',
  title: lang('title'),
  type: 'document',
  // In order to limit config to only one document (except when first created)
  __experimental_actions: [/* 'create', */ 'update', /* 'delete', */ 'publish'],

  fields: [
    {
      name: 'name',
      title: lang('fields', 'name'),
      type: 'slug',
    },

    ...getLocaleContentObj(),

    {
      name: 'mainImage',
      title: 'Main image',
      type: 'image',
      options: {
        hotspot: true,
      },
    },


  ],

  preview: {
    select: {
      title: 'name.current'
    }
  }

};

export default generalContent;