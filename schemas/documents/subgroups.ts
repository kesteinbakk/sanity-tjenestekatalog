import { getLangFunc, /* defLangId */ } from "../../language/lang";
import { defaultLang } from "../../settings";
import { getLocaleContentObj } from "../locale/localeContent";

const lang = getLangFunc('subgroups');

const subGroups = {
  name: 'subgroups',
  title: lang('title'),
  type: 'document',
  fields: [

    {
      name: 'category',
      title: lang('fields', 'category'),
      type: 'reference',
      to: [{ type: 'categories' }],
      description: lang('fields', 'categoryDescription'),
      validation: Rule => Rule.required(),
    },

    ...getLocaleContentObj(),


    {
      name: 'image',
      title: lang('fields', 'image'),
      type: 'image',
      options: {
        hotspot: true,
      },
    },

  ],

  preview: {
    select: {
      title: `${defaultLang}Content.title`,
      //title: 'localeName',
      // subtitle: '',
      media: 'image'
    },
    /* prepare(selection) {
      console.log('selectrion cat', selection)
      const { title, subtitle } = selection;
      return {
        title,
        subtitle
      }
    }, */
  }
};

export default subGroups;