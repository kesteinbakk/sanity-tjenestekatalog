import { getLangFunc } from "../../language/lang";
import { defaultLang } from "../../settings";
import { getLocaleContentObj } from "../locale/localeContent";


const lang = getLangFunc('serviceLevel');

const serviceLevel = {
  name: 'serviceLevel',
  title: lang('title'),
  type: 'document',
  fields: [
    ...getLocaleContentObj('serviceLevelContent')
  ],
  preview: {
    select: {
      title: `${defaultLang}ServiceLevelContent.title`,
      // subtitle: ''
    }
  }
};

export default serviceLevel;