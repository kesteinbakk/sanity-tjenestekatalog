import { getLangFunc } from "../../language/lang";
import { getLocaleContentObj } from "../locale/localeContent";

const lang = getLangFunc('customers');
//const langs = getLangsFunc('customers')
// console.debug("IN CUSTOMER", getLocaleContentObj('customerContent'))
const customers = {
  name: 'customers',
  title: lang('title'),
  type: 'document',
  fields: [
    {
      name: 'name',
      title: lang('name'),
      type: 'string',
    },
    {
      name: 'type',
      type: 'string',
      options: {
        list: [
          { title: lang('fields', 'muni'), value: 'muni' },
          { title: lang('fields', 'otherPublic'), value: 'otherPublic' },
          { title: lang('fields', 'private'), value: 'private' },
          { title: lang('fields', 'other'), value: 'other' }
        ],
      },
      initialValue: 'muni',
      validation: ((Rule) => Rule.required())
    },
    ...getLocaleContentObj('customerContent'),


  ],
};

export default customers;