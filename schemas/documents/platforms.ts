import { getLangFunc } from "../../language/lang";

const lang = getLangFunc('platforms');

const platforms = {
  name: 'platforms',
  title: lang('title'),
  type: 'document',
  fields: [

    {
      name: 'name',
      title: lang('fields', 'name'),
      type: 'string',
      validation: Rule => Rule.required(),
    },

    {
      name: 'description',
      title: lang('fields', 'description'),
      type: 'blockContent',
    },

    {
      name: 'serviceProvider',
      title: lang('fields', 'serviceProvider'),
      type: 'string',
    },
    {
      name: 'logo',
      title: lang('fields', 'logo'),
      type: 'image',
      options: {
        hotspot: true,
      },
    },
  ],

  preview: {
    select: {
      title: 'name',
      subtitle: 'serviceProvider',
      media: 'logo'
    },
  }
};

export default platforms;