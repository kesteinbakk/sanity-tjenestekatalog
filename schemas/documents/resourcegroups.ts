import { getLangFunc, /* defLangId */ } from "../../language/lang";

const lang = getLangFunc('resourcegroups');

const resourcegroups = {
  name: 'resourcegroups',
  title: lang('title'),
  type: 'document',
  fields: [

    {
      name: 'name',
      title: lang('fields', 'name'),
      type: 'string',
      validation: Rule => Rule.required(),
    },

    {
      name: 'description',
      title: lang('fields', 'description'),
      type: 'blockContent',
    }

  ],

  preview: {
    select: {
      title: 'name',
    },
  }
};

export default resourcegroups;