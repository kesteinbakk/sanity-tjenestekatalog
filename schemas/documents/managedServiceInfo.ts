import { getLangFunc } from "../../language/lang";

const lang = getLangFunc('managedServiceInfo');

const managedServiceInfo = {
  name: 'managedServiceInfo',
  title: lang('title'),
  type: 'document',
  fields: [
    {
      name: 'service',
      title: lang('fields', 'service'),
      type: 'reference',
      to: [
        { type: 'services' }
      ]
    },

    {
      name: 'serviceStatus',
      title: lang('fields', 'serviceStatus'),
      type: 'string',
      initialValue: 'ok',
      options: {
        list: [
          { title: '✅ - ' + lang('fields', 'ok'), value: 'ok' },
          { title: '⚠️ - ' + lang('fields', 'warning'), value: 'warning' },
          { title: '🚫 - ' + lang('fields', 'stopped'), value: 'stopped' }
        ]
      }
    },

    {
      name: 'log',
      title: lang('fields', 'log'),
      type: 'array',
      of: [{ type: 'logItems' }]
    },

  ],
  preview: {
    select: {
      title: 'service.name.current',
      subtitle: 'serviceStatus' || '',
      media: 'service.media '
    },

  }
};

export default managedServiceInfo;