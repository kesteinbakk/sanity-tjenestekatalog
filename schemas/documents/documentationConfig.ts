//import client from 'part:@sanity/base/client'
import { getLangFunc } from "../../language/lang";

const lang = getLangFunc('documentation', 'config');

const explanationConfig = {
  name: 'documentationConfig',
  title: lang('title'),
  type: 'document',
  __experimental_actions: ['update', 'publish'],

  fields: [
    {
      name: 'name',
      title: lang('name'),
      type: 'slug'
    },

    {
      name: 'defaultImage',
      title: lang('defaultImage'),
      type: 'image',
      options: {
        hotspot: true,
      },
    },


  ],

  preview: {
    select: {
      title: 'name.current'
    },
    prepare: ({ title }) => ({
      title
    })
  }


};

export default explanationConfig;
