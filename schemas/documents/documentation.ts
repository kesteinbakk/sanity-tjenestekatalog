//import client from 'part:@sanity/base/client'
import { getLangFunc } from "../../language/lang";
import { defaultLang } from "../../settings";
import localeContent from "../locale/localeDocumentationContent";
const lang = getLangFunc('documentation');

const documentation = {
  name: 'documentation',
  title: lang('title'),
  type: 'document',
  initialValue: () => ({
    publishedAt: new Date().toISOString(),
    /* userGroup: [
      {
        _type: "userGroupReference",
        userGroup: {
          _ref: "6dbecd25-6f17-4b28-8c94-2cd8db8383c4",
          _type: "reference"
        }
      }
    ] */
  }),
  fieldsets: [
    {
      name: 'translations',
      title: lang('translations'),
      options: {
        collapsible: true,
        collapsed: true,
        // columns: 2
      }
    },


  ],
  fields: [
    ...localeContent.map(item => ({
      name: item.name,
      title: item.title,
      type: item.name,
      fieldset: defaultLang === item.langId ? null : 'translations',
    })),


    {
      name: 'image',
      title: lang('fields', 'image'),
      type: 'image',
      options: {
        hotspot: true,
      },
    },

    {
      name: 'publishedAt',
      title: lang('fields', 'publishedAt'),
      type: 'datetime',
      validation: Rule => Rule.required(),
    },
  ],
  orderings: [
    {
      title: 'Title',
      name: 'title',
      by: [{ field: `${defaultLang}ServiceContent.title`, direction: 'asc' }]

    }
  ],
  preview: {
    select: {
      title: `${defaultLang}DocumentationContent.title`,
      subtitle: `${defaultLang}DocumentationContent.subtitle`,
      media: 'image',
    },
    prepare(selection: {
      title: string,
      subtitle: string,
      media: any
    }) {
      // console.debug('sel in service', selection);
      const { title, subtitle, media } = selection;

      return {
        title: title || "---",
        subtitle,
        media,
      };
    },
  },
};

export default documentation;
