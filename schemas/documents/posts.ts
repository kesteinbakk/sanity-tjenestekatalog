import { getLangFunc } from "../../language/lang";
import { defaultLang } from "../../settings";
import { getLocaleContentObj } from "../locale/localeContent";

const lang = getLangFunc('posts');

const articles = {
  name: 'posts',
  title: lang('title'),
  type: 'document',
  fields: [
    {
      name: 'name',
      title: lang('fields', 'name'),
      type: 'slug',
    },
    ...getLocaleContentObj(),
    {
      name: 'mainImage',
      title: lang('fields', 'mainImage'),
      type: 'image',
      options: {
        hotspot: true,
      },
    },

    {
      name: 'publishedAt',
      title: lang('fields', 'publishedAt'),
      type: 'datetime',
      validation: Rule => Rule.required(),
      initialValue: (new Date()).toISOString(),
    },
  ],

  preview: {
    select: {
      title: 'name.current',
      subtitle: `${defaultLang}Content.title`,
      media: 'mainImage',
    },
    prepare(selection) {
      const { title, subtitle, media } = selection;

      //console.debug('SUB', subtitle, defaultLang)
      //  const contentEntries = content && Object.entries(content);
      //   const langs = contentEntries?.filter(([key, value]) =>
      //   key != '_type' && value?.title !== ""
      //).map(([key]) => key)
      return {
        title: title || '---',
        subtitle: subtitle || "---",
        image: media
      };
    },
  },
};

export default articles;