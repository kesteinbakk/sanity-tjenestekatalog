import { getLangFunc } from "../../language/lang";
import ManagementInput from '../../components/management/CustomersServicesInput';
const lang = getLangFunc('customersServices');

const customersServices = {
  name: 'customersServices',
  title: lang('title'),
  type: 'document',
  fields: [
    {
      name: 'customer',
      title: lang('fields', 'customer'),
      type: 'reference',
      readOnly: true,
      to: [
        { type: 'customers' },
      ]
    },

    {
      name: 'services',
      title: lang('customersServices'),
      type: 'array',
      of: [{ type: 'string' }],
      options: {
        id: 'test'
      },
      inputComponent: ManagementInput,
    },
    /*  {
       name: 'managedServicesInfo',
       title: lang('fields', 'managedServicesInfo'),
       type: 'array',
       of: [
         {
           type: 'managedService',
         },
       ]
     }, */



  ],
  preview: {
    select: {
      title: 'customer.name',
      services: 'customersServices'
    },

    prepare: (select: {
      title: string,
      services: string[]
    }) => {
      const { title, services } = select;
      return {
        title,
        subtitle: services.length
      }
    }

  }
};

export default customersServices;