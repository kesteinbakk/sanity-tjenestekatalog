import { getLangFunc } from "../../language/lang";

const lang = getLangFunc('status');

const status = {
  name: 'status',
  title: lang('title'),
  type: 'document',
  fields: [
    {
      name: 'service',
      title: lang('fields', 'service'),
      type: 'reference',
      to: [
        { type: 'services' }
      ]
    },

    {
      name: 'log',
      title: lang('fields', 'log'),
      type: 'array',
      of: [{ type: 'statusLogItems' }]
    },

  ],
  preview: {
    select: {
      title: 'service.name.current',
      subtitle: 'serviceStatus' || '',
      media: 'service.media '
    },

  }
};

export default status;