import { getLangFunc } from "../../language/lang";
import { defaultLang } from "../../settings";
import { getLocaleContentObj } from "../locale/localeContent";

const lang = getLangFunc('categories');

const categories = {
  name: 'categories',
  title: lang('title'),
  type: 'document',
  fields: [
    /*  {
       name: 'name',
       title: lang('fields', 'name'),
       type: 'string',
       validation: Rule => Rule.required().min(2).max(30)
     },
  */
    ...getLocaleContentObj(),

    {
      name: 'priority',
      title: lang('fields', 'priority'),
      type: 'number'
    },

    {
      name: 'image',
      title: lang('fields', 'imageWithText'),
      type: 'image',
      options: {
        hotspot: true,
      },
    },

    {
      name: 'imageNoText',
      title: lang('fields', 'imageNoText'),
      type: 'image',
      options: {
        hotspot: true,
      },
    },
    {
      name: 'imageTitle',
      title: lang('fields', 'imageTitle'),
      type: 'string',
      validation: (Rule) => Rule.max(20)
    }
  ],

  preview: {
    select: {
      title: `${defaultLang}Content.title`,
      //title: 'localeName',
      // subtitle: 'categories.name',
      media: 'image'
    },
  }
};

export default categories;