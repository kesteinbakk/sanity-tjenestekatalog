import { getLangFunc } from "../../language/lang";
import { getLocaleContentObj } from "../locale/localeContent";
import { truncate } from "../../utils/utils";
import { defaultLang } from "../../settings";


const lang = getLangFunc('logItems');

const statusLogItems = {
  name: 'statusLogItems',
  title: lang('title'),
  type: 'document',
  fields: [
    {
      name: 'date',
      title: lang('fields', 'date'),
      type: 'datetime',
      validation: Rule => Rule.required(),
      initialValue: (new Date()).toISOString()
    },
    {
      name: 'serviceStatus',
      title: lang('fields', 'serviceStatus'),
      type: 'string',
      initialValue: 'ok',
      options: {
        list: [
          { title: '✅ - ' + lang('fields', 'success'), value: 'success' },
          { title: '⚠️ - ' + lang('fields', 'warning'), value: 'warning' },
          { title: '🚫 - ' + lang('fields', 'error'), value: 'error' },
          { title: lang('fields', 'info'), value: 'info' },
        ]
      }
    },
    ...getLocaleContentObj(),

  ],
  orderings: [
    {
      name: 'dateNewestFirst',
      title: lang('order', 'newestFirst'),
      by: [
        { field: 'date', direction: 'desc' }
      ]
    },
    /*     {
          name: 'dateOldestFirst',
          title: lang('order', 'oldestFirst'),
          by: [
            { field: 'date', direction: 'desc' }
          ]
        }, */
  ],
  preview: {
    select: {
      title: `${defaultLang}Content.title`,
      content: `${defaultLang}Content.content`,
      date: 'date'
    },
    prepare: (props) => {
      const { title, date, content } = props;
      console.log('Content:', content);
      const dateOptions: { day: "numeric", month: "short", year: "numeric" | "2-digit" } = {
        day: 'numeric',
        month: 'short',
        year: 'numeric'
      };
      const localeDate = new Date(date)
        .toLocaleDateString(defaultLang, dateOptions);
      const contentPart = content?.[0]?.children[0]?.text || '--';
      return {
        title: `${title} (${localeDate})`,
        subtitle: truncate(contentPart, 50)
      };
    }

  }
};

export default statusLogItems;