//import client from 'part:@sanity/base/client'
import { getLangFunc } from "../../language/lang";
import { defaultLang } from "../../settings";
import localeServiceContent from "../locale/localeServiceContent";

const lang = getLangFunc('services');

const services = {
  name: 'services',
  title: lang('title'),
  type: 'document',
  initialValue: () => ({
    publishedAt: new Date().toISOString(),
    /* userGroup: [
      {
        _type: "userGroupReference",
        userGroup: {
          _ref: "6dbecd25-6f17-4b28-8c94-2cd8db8383c4",
          _type: "reference"
        }
      }
    ] */
  }),
  fieldsets: [
    {
      name: 'translations',
      title: lang('translations'),
      options: {
        collapsible: true,
        collapsed: true,
        // columns: 2
      }
    },
    {
      name: 'metaData',
      title: lang('metaData'),
      options: {
        collapsible: true,
        collapsed: true,
      }
    },
    /*  {
       name: 'resourcegroup',
       title: lang('fields', 'resourcegroup'),
       options: {
         collapsible: true,
         collapsed: true,
       }
     } */
  ],
  fields: [
    {
      name: 'itemNumber',
      title: lang('fields', 'itemNumber'),
      type: 'slug',
    },


    {
      name: 'resourceGroups',
      title: lang('fields', 'resourcegroup'),
      type: 'array',
      of: [{
        type: 'reference',
        to: [{ type: 'resourcegroups' }],
      }],
      fieldset: 'metaData'
    },

    {
      name: 'platform',
      title: lang('fields', 'platform'),
      type: 'reference',
      to: [{ type: 'platforms' }],
      fieldset: 'metaData'
    },
    {
      name: 'itemName',
      title: lang('fields', 'itemName'),
      type: 'string',
      fieldset: 'metaData'
    },
    {
      name: 'appName',
      title: lang('fields', 'appName'),
      type: 'string',
      fieldset: 'metaData',
    },



    {
      name: 'category',
      title: lang('fields', 'category'),
      type: 'reference',
      to: { type: 'categories' },
      options: {
        disableNew: false,
      },
      validation: Rule => Rule.required(),
    },

    ...localeServiceContent.map(item => ({
      name: item.name,
      title: item.title,
      type: item.name,
      fieldset: defaultLang === item.langId ? null : 'translations',
    })),

    {
      name: 'mandatory',
      title: lang('fields', 'mandatory'),
      type: 'boolean',
      initialValue: false,
    },

    {
      name: 'mainImage',
      title: lang('fields', 'mainImage'),
      type: 'image',
      options: {
        hotspot: true,
      },
    },

    {
      name: 'publishedAt',
      title: lang('fields', 'publishedAt'),
      type: 'datetime',
      validation: Rule => Rule.required(),
    },
  ],
  orderings: [
    {
      title: 'Title',
      name: 'title',
      by: [{ field: `${defaultLang}ServiceContent.title`, direction: 'asc' }]
    },
    {
      title: 'Category',
      name: 'category',
      by: [{ field: `category.${defaultLang}Content.title`, direction: 'asc' }]
    }
  ],
  preview: {
    select: {
      title: `${defaultLang}ServiceContent.title`,
      cat: `category.${defaultLang}Content.title`,
      // content: 'content',//`title.${baseLanguage.id}`
      media: 'mainImage',
      secMedia: 'categoryOfService.image',
      thirdMedia: 'typeOfService.image',

    },
    prepare(selection) {
      // console.debug('sel in service', selection);
      const { title, cat, media, secMedia, thirdMedia } = selection;

      return {
        title: title || "---",
        subtitle: cat,
        media: media || secMedia || thirdMedia
      };
    },
  },
};

export default services;
