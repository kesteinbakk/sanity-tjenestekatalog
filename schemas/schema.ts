// First, we must import the schema creator
import createSchema from 'part:@sanity/base/schema-creator';

// Then import schema types from any plugins that might expose them
import schemaTypes from 'all:part:@sanity/base/schema-type';

// Schemas
import categories from './documents/categories';
import services from './documents/services';
// import webPageLanguage from './documents/webPageLanguage';
// import webPageConfig from './documents/webPageConfig';
// import managedServiceInfo from './documents/managedServiceInfo';
import customers from './documents/customers';
import platforms from './documents/platforms';
import serviceLevel from './documents/serviceLevel';

// Locale support:
import { customerContent, localeContent, serviceLevelContent } from './locale/localeContent';

import { localeString } from './locale/localeString';

// Other support
import blockContent from './blockContent';
// import statusLogItems from './documents/statusLogItems';
import resourcegroups from './documents/resourcegroups';
import localeServiceContent from './locale/localeServiceContent';
import localeDocumentationContent from './locale/localeDocumentationContent';
import documentation from './documents/documentation';
import explanationConfig from './documents/documentationConfig';
import customersServices from './documents/customersServices';
import generalContent from './documents/generalContent';


export default createSchema({
  // We name our schema
  name: 'default',
  // Then proceed to concatenate our document type
  // to the ones provided by any plugins that are installed
  types: schemaTypes.concat([
    services,

    documentation,
    explanationConfig,
    generalContent,

    customersServices,
    // posts,

    categories,
    serviceLevel,
    platforms,
    resourcegroups,
    // subgroups,

    customers,

    ...customerContent,
    ...localeContent,
    ...localeServiceContent,
    ...localeDocumentationContent,
    ...serviceLevelContent,
    localeString,
    blockContent,

    // webPageLanguage,
    // webPageConfig,

    // status,
    // statusLogItems,


  ]),
})
