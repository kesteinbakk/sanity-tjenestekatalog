import React, { useState, useEffect, useCallback, useMemo } from 'react';
import { Card, /* Input, */ Stack, Flex, Checkbox, Box, Text, TextInput, Button,/*  Tooltip */ } from '@sanity/ui';
import { FormField } from '@sanity/base/components';
import PatchEvent, { set, /* unset  */ } from '@sanity/form-builder/PatchEvent';

//import { defLangId } from '../language/lang'
import sanityClient from 'part:@sanity/base/client';

import { MdKeyboardArrowRight, MdKeyboardArrowDown } from 'react-icons/md';
//import { TiDelete } from 'react-icons/ti';

import { getLangFunc } from '../../language/lang';
import demoData from './demoDataForManComp';
import MyIcon from './MyIcon';
import { storageKeys, useDemoData, apiVersion, defaultLang } from '../../settings'
import localStorage from '../../utils/localeStorage';
import { filterDrafts, sortByStringOrNumber } from '../../utils/utils';

const client = sanityClient.withConfig({ apiVersion });
//const content = `${defLangId}ServiceContent`
const lang = getLangFunc('managementInput');

const localeStorageKey = storageKeys.management;

const servicesOnCategoriesQuery = `*[_type == "categories"] {
_id, 
priority, 
"title":${defaultLang}Content.title,
"services": *[_type == "services"
&& references(^._id)]{
  _id,
  "subgroupTitle": subgroup->${defaultLang}Content.title,
  "serviceTitle": ${defaultLang}ServiceContent.title,
  mandatory
  }
}
`;



// mainImage, 
const params = {};

type ServiceData = {
  _id: string,
  serviceTitle: string | null,
  subgroupTitle: string | null,
  mandatory: boolean,
}

export type ServicesOnCategoriesData = {
  _id: string,
  title: string,
  priority: number,
  "services": ServiceData[]
}

type Props = {
  type: { options: any, description: string, title: string },
  value: string[],
  markers: any,
  presence: any,
  compareValue: any,
  onChange: (value: PatchEvent) => void,
  parent: { _id: string }
}

const ServiceManagementInput = React.forwardRef((props: Props, ref: React.Ref<HTMLDivElement>) => {

  const {
    type,         // Schema information
    value = [],        // Current field value
    // readOnly,     // Boolean if field is not editable
    markers,      // Markers including validation rules
    presence,     // Presence information for collaborative avatars
    compareValue, // Value to check for "edited" functionality
    // onFocus,      // Method to handle focus state
    //  onBlur,       // Method to handle blur state  
    onChange,      // Method to handle patch events
    parent: { _id: parentId }
  } = props;

  // const { options } = type;
  const expandedKey = useMemo(() => localeStorageKey + parentId, [parentId]);

  // Hooks
  const [serviceData, setServiceData] = useState<ServicesOnCategoriesData[]>(useDemoData ? demoData : []);
  const [expanded, setExpanded] = useState<{ [key: string]: boolean }>(useDemoData ? {
    '023f14ad-4259-4a85-b8ed-795967a05bb7': true
  } : localStorage.get(expandedKey) || {});
  const [searchStr, setSearchStr] = useState('');
  //const [tempChecked, setTempChecked] = useState(value)


  //console.log('all props', props, 'ID', options.id);

  useEffect(() => {
    const getServices = async () => {

      !useDemoData && client.fetch(servicesOnCategoriesQuery, params).then((data: ServicesOnCategoriesData[]) => {
        //  filter out drafts
        setServiceData(data
          .filter(item => filterDrafts(item._id))
          .sort((a, b) => sortByStringOrNumber(a.priority, b.priority, true),)
          .map(item => {
            const { services } = item;
            return {
              ...item,
              services: services.filter(item => filterDrafts(item._id))
            };
          }));


      });
      //const category = 
      /*    setServiceData(data.reduce((prev, item, index) => {
           prev[item._id] = prev[item._id] || {
             label: item.title | '---',
             index,
             nodes: {}
           }
           prev[item._id].nodes[item.services._id] = {
             label: item.services.serviceName || '---'
           }
           return prev
 
         }, {}))
         console.info('ServiceData is', serviceData) */
    };

    getServices();
  }, []);

  //console.log('Service data is', serviceData);

  const handleChange = useCallback(
    // useCallback will help with performance
    (event) => {
      const { checked, id: serviceId } = event.currentTarget; // get current value

      if (checked) {
        if (!value.includes(serviceId)) {
          value.push(serviceId);
        }
      } else {
        if (value.includes(serviceId)) {
          value.splice(value.indexOf(serviceId), 1);
        }
      }

      // if the value exists, set the data, if not, unset the data
      onChange(PatchEvent.from(set([...value])));
    },
    [onChange, value]
  );

  const handleExpandingFunc = useCallback((id) => () => {
    setExpanded((current) => {

      const newObj = {
        ...current,
        [id]: !current[id]
      };
      localStorage.set(expandedKey, newObj);
      return newObj;
    });
  }, [expandedKey]);

  const clearAll = () => onChange(PatchEvent.from(set([])));

  // filter func
  // Returns true if no searchStr or searchStr is included in textToSearch
  const filterFunc = useCallback((textToSearch = '') => searchStr === ''
    || !textToSearch
    || textToSearch.toLowerCase().search(searchStr.toLowerCase()) !== -1,
    [searchStr]);

  const filteredData = serviceData.map((item) => {
    const { services } = item;
    const filteredServices = services.filter(({ serviceTitle }) => filterFunc(serviceTitle));
    return {
      ...item,
      services: filteredServices,
    };
  }, []);




  /*   const IconTooltip = ({ text, children }) => (
      <Tooltip
        content={
          <Box padding={2}>
            <Text muted size={1}>
              {text}
            </Text>
          </Box>
        }
        fallbackPlacements={['right', 'left']}
        placement="top"
        portal
        children={children}
      />
    ); */

  const ServiceHeader = useCallback(({ title, noOfServices, noOfCheckedServices, _id }) => {

    const ExpandedIcon = ({ size, _id }: { size: string, _id: string }) => {
      const handleExpanding = handleExpandingFunc(_id);
      return expanded[_id] ?
        <MdKeyboardArrowDown color="green" size={size} onClick={handleExpanding} /> :
        <MdKeyboardArrowRight size={size} onClick={handleExpanding} />;
    };

    const count = `${noOfCheckedServices}/${noOfServices}`;
    return (
      <Card padding={2}>
        <Flex align="center">
          <ExpandedIcon size="2em" _id={_id} />
          <Text size={3} weight="regular">
            {title || `[${lang('noTitle')}!]`} ({count})</Text>
        </Flex>
      </Card>
    );
  }, [expanded, handleExpandingFunc]);

  const ServiceItem = useCallback(({ title, mandatory, _id, checked = false }) => (
    <Stack>
      <Card padding={4}>
        <Flex align="center">
          <Checkbox
            id={_id}
            style={{ display: 'block' }}
            defaultChecked={checked}
            onChange={handleChange}
          />
          <Box paddingX={3}>
            <Text
              muted={!checked}
            >
              <label htmlFor="checkbox">{title}</label>
            </Text>
          </Box>

          {mandatory && !checked && MyIcon(
            {
              name: 'exclamation',
              color: 'orange',
              hint: lang('icons', 'mandatoryHint'),
              info: {
                text: lang('icons', 'mandatoryInfo'),
                status: 'warning'
              }
            })
          }
        </Flex>
      </Card>

    </Stack>
  ), [handleChange]);


  // Output

  if (!serviceData) {
    return <div>Loading data...</div>;
  }

  return (
    <FormField
      description={type.description}  // Creates description from schema
      title={type.title}              // Creates label from schema title
      __unstable_markers={markers}    // Handles all markers including validation
      __unstable_presence={presence}  // Handles presence avatars
      // @ts-ignore
      compareValue={compareValue}     // Handles "edited" status
    //id="serviceManagementInput"
    >
      <Card padding={2} width="100%" radius={2} shadow={1}>
        <Stack space={[3, 3, 4]}>
          <Flex width="100%" justify={"space-between"} ref={ref}>
            <TextInput
              placeholder={lang('filter')}
              value={searchStr}
              onChange={(event) => {
                setSearchStr(event.currentTarget.value);
              }} />

            <Button
              tone="critical"
              fontSize={[1, 1, 2]}
              width='400px'
              //  icon={TiDelete}
              onClick={clearAll}
              padding={1}
            >
              {lang('buttons', 'removeAll')}
            </Button>
          </Flex>
          {filteredData.length ? filteredData
            .map(({ services, _id, title }) => {
              if (!services.length) {
                // If there are no services for this servicetype...
                return null;
              }
              // console.log('Printing id', _id, 'expanded is', expanded,
              // 'services are', services);

              const itemExp = expanded[_id];
              return (
                <Stack key={_id}>
                  <ServiceHeader
                    title={title}
                    noOfServices={services.length}
                    noOfCheckedServices={services.filter(item => value.includes(item._id)).length}
                    _id={_id}
                  />
                  {(itemExp || searchStr !== '') && services
                    .map(({ _id, mandatory, serviceTitle, }) => (
                      <ServiceItem
                        key={_id}
                        title={serviceTitle}
                        _id={_id}
                        mandatory={mandatory}
                        checked={value && value.includes(_id)}
                      />
                    ))}

                </Stack>

              );
            })
            : <Text>{lang('noServices')} </Text>}

        </Stack>
      </Card>
    </FormField>
  );
});

export default ServiceManagementInput;
