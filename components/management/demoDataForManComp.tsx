import { ServicesOnCategoriesData } from "./CustomersServicesInput";

const demoData: ServicesOnCategoriesData[] = [
  {
    "_id": "023f14ad-4259-4a85-b8ed-795967a05bb7",
    "priority": 1,
    "services": [
      {
        "_id": "5c92198f-b683-4887-be2a-6da3119fa9de",
        "subgroupTitle": null,
        "mandatory": true,
        "serviceTitle": "Internettrafikk med sikkerhetstjenester"
      }
    ],
    "title": "Ikomm ONPREM Datasentertjenester"
  },
  {
    "_id": "02bbd10c-3978-4b65-a443-969c41baf8c9",
    "priority": 1,
    "services": [],
    "title": "Ikomm Driftstjenester"
  },
  {
    "_id": "0565550b-62a5-4fc8-8a3c-b6793f28fbfb",
    "priority": 1,
    "services": [],
    "title": "Ikomm KUNDE lokasjonstjenester"
  },
  {
    "_id": "0aa541e8-94b4-483e-a6d1-2b45474f6376",
    "priority": 1,
    "services": [
      {
        "_id": "6ba0cc13-040a-499b-8f0c-c3eb206e2c65",
        "subgroupTitle": "Ikomm Allegro Delte tjenester",
        "mandatory": true,
        "serviceTitle": "Azure Firewall"
      },
      {
        "_id": "8b1c929d-317f-45ab-b750-04350a25a80b",
        "subgroupTitle": "Ikomm Allegro Delte tjenester",
        "mandatory": true,
        "serviceTitle": "Azure Felles Nettverk"
      },
      {
        "_id": "e26501bb-f91e-4cb7-96b5-3178b9b0e5d7",
        "subgroupTitle": "Ikomm Allegro Delte tjenester",
        "mandatory": true,
        "serviceTitle": "Sentral internettaksess"
      },
      {
        "_id": "fd3a73d4-bf89-4117-9009-04dff4c46093",
        "subgroupTitle": "Azure Kundededikerte tjenester",
        "mandatory": true,
        "serviceTitle": "Azure Abonnement (Subsciptions)"
      }
    ],
    "title": "Ikomm Allegro datasentertjenester"
  },
  {
    "_id": "123e246e-7347-4fac-8154-6204737fa6c9",
    "priority": 1,
    "services": [],
    "title": "Avtaleforvaltning (eksterne tjenester)"
  },
  {
    "_id": "383d1b6b-545d-47fb-90ba-531627861e0f",
    "priority": 1,
    "services": [],
    "title": "Ikomm Sikkerhetstjenester"
  },
  {
    "_id": "9689aad6-24a7-440d-ad0b-e1945de42cd9",
    "priority": 1,
    "services": [],
    "title": "Ikomm WEB-shop"
  },
  {
    "_id": "ea6c6322-9beb-407e-8bc5-1cb444681e42",
    "priority": 1,
    "services": [],
    "title": null
  }
];

export default demoData;