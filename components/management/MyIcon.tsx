import React from 'react';

import { AiOutlineExclamationCircle } from 'react-icons/ai';

import { useToast } from '@sanity/ui';

type SupportedIcons = 'exclamation'

const getElement = (name: SupportedIcons) => {
  switch (name) {
    case 'exclamation': return AiOutlineExclamationCircle;
    default: throw Error('Unknown icon: ' + name);
  }
};

type Props = {
  name: SupportedIcons,
  color: string,
  onClick?: () => void,
  hint: string,
  info: {
    status: 'success' | 'error' | 'warning' | 'info',
    text: 'string'
  }
}

const MyIcon = ({ name, color, onClick, hint, info }: Props) => {
  const Element = getElement(name);
  const toast = useToast();
  const infoClick = info ? () =>
    toast.push({
      status: info.status,//'success',
      title: info.text
    }) : null;


  return <Element
    title={hint}
    color={color}
    onClick={onClick || infoClick || undefined}
  />;
};

export default MyIcon;