import { defaultLang } from "../settings";
import config from "./configLang";

import services from './langForSchemas/servicesLang';
import managementInput from "./managementInputLang";
// import posts from './langForSchemas/postsLang';
// import status from './langForSchemas/statusLang';
import categories from './langForSchemas/categoriesLang';
// import subgroups from "./langForSchemas/subgroupsLang";
import customers from "./langForSchemas/customersLang";
import serviceLevel from "./langForSchemas/serviceLevelLang";
import platforms from "./langForSchemas/platformsLang";
import documentation from "./langForSchemas/documentationLang";
import resourcegroups from "./langForSchemas/resourcegroupsLang";
import generalContent from "./langForSchemas/generalContentLang";
// import { webPageLanguage, webPageConfig } from "./langForSchemas/webPageLang&Config";

import localeContent from './langForSchemas/localeLang/localeContentLang';
import localeString from "./langForSchemas/localeLang/localeStringLang";
import customersServices from "./langForSchemas/customersServicesLang";
// import serviceManagementInput from "./serviceManagementInputLang";
//import managedServiceInfo from "./langForSchemas/managedServiceInfoLang";
// import logItems from "./langForSchemas/statusLogItemsLang";
//import logItems from "./langForSchemas/logItemsLang";

export const lang = {
  services,
  documentation,
  // posts,
  customersServices,
  managementInput,
  customers,
  generalContent,
  // status,
  categories,
  serviceLevel,
  platforms,
  resourcegroups,
  // subgroups,
  localeContent,
  localeString,
  config,
  // webPageConfig,
  // webPageLanguage,
  // logItems,
};


const reportMissingKey = (key: string, keys: string[]) => {
  console.error('Lang error! Keys was', keys, 'Language object is', lang, 'Failed on', key);
  throw Error(`No language for keys "${keys.join(', ')}" (language "${defaultLang}"). Failed on key: ${key}`);
};

const reportInvalidKey = (key: any, keys: any[]) => {
  console.error('Invalid key:', key);
  const conMsg = `In array ${keys}`;
  throw Error(`Invalid language key. Got: ${key} (${typeof key}). ${conMsg}`);
};

export const getLangs = (...keys: string[]) => keys.reduce((prev, cur) => {
  typeof cur == 'string' || reportInvalidKey(cur, keys);
  return prev[cur] || reportMissingKey(cur, keys);
}, lang);

export const getDefLang = (...keys: string[]) => getLangs(...keys)[defaultLang];

export const getAllLangsFunc = (...initKeys: string[]) =>
  (...keys) => getLangs(...[...initKeys, ...keys]);

export const getLangFunc = (...initKeys: string[]) =>
  (...keys) => getDefLang(...[...initKeys, ...keys]);

/*   const customLang = keys.reduce((prev, cur) =>
    prev[cur], lang) || reportError(keys, customLang); */
