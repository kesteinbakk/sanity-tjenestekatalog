export default {
  name: {
    en: 'Name',
    no: 'Navn'
  },
  translations: {
    en: 'Translations',
    no: 'Oversettelser'
  },
  content: {
    en: 'Content',
    no: 'Innhold'
  },
  fields: {
    name: {
      en: 'Name',
      no: 'Navn'
    },
    title: {
      en: 'Title',
      no: 'Tittel'
    },
    subtitle: {
      en: 'Subtitle',
      no: 'Undertittel'
    },
    description: {
      en: 'Description',
      no: 'Beskrivelse'
    },
    content: {
      en: 'Content',
      no: 'Innhold'
    },
    image: {
      en: 'Image',
      no: 'Bilde'
    },
    publishedAt: {
      en: 'Published at',
      no: 'Publisert dato'
    },
  },
  languages: {
    no: {
      no: 'Norsk',
      en: 'Norwegian'
    },
    en: {
      no: 'Engelsk',
      en: 'English'
    },
  },
  customers: {
    lillehammer: {
      no: 'Lillehammer kommune',
      en: 'Lillehammer municipality'
    },
    oyer: {
      no: 'Øyer kommune',
      en: 'Øyer municipality'
    },
    gausdal: {
      no: 'Gausdal kommune',
      en: 'Guasdal municipality'
    },
    indreOstfold: {
      no: 'Indre Østfold kommune',
      en: 'Indre Østfold municipality'
    },
    nesodden: {
      no: 'Nesodden kommune',
      en: 'Nesodden municipality'
    },
    ostreToten: {
      no: 'Østre Toten kommune',
      en: 'Østre Toten municipality'
    },
    valer: {
      no: 'Våler kommune',
      en: 'Våler municipality'
    }
  }
}