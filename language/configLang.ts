const configLang = {

  title: {
    en: 'Content for Ikomm',
    no: 'Innhold for Ikomm',
  },

}

export default configLang;