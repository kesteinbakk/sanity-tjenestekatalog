const lang = {

  icons: {
    mandatoryHint: {
      en: "Mandatory service",
      no: "Obligatorisk tjeneste",
    },
    mandatoryInfo: {
      en: "This service is mandatory but has not yet been activated for this customer!",
      no: "Denne tjenesten er obligatorisk, men har enda ikke blitt aktivert for denne kunden"
    },

  },

  buttons: {
    removeAll: {
      en: 'Remove all',
      no: 'Fjern alle'
    }
  },
  filter: {
    en: "Filter",
    no: "Filtrer",
  },


  noTitle: {
    en: "Missing title",
    no: "Mangler tittel"
  },

  noServices: {
    en: "No services",
    no: "Ingen tjenester"
  },

};

export default lang;