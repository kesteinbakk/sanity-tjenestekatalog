import generalLang from "../generalSchemaLang";


const postsLang = {
  ...generalLang,

  title: {
    en: 'Posts',
    no: 'Poster',
  },
  fields: {
    ...generalLang.fields,
    name: {
      en: 'Name',
      no: 'Navn'
    },
    userGroup: {
      en: 'User group',
      no: 'Brukergruppe'
    },
    content: {
      en: 'Content',
      no: 'Innhold'
    },

    mainImage: {
      en: 'Main image',
      no: 'Bilde'
    },

    publishedAt: {
      en: 'Published at',
      no: 'Publisert dato'
    },
  },

  preview: {
    missingUserGroup: {
      en: 'Missing user group',
      no: 'Mangler brukergruppe'
    },
    langs: {
      en: 'Langs',
      no: 'Språk'
    },
  }
};


export default postsLang;