import generalLang from "../generalSchemaLang";

const categoriesLang = {
  ...generalLang,
  title: {
    en: 'Categories',
    no: 'Kategorier'
  },
  fields: {
    ...generalLang.fields,
    priority: {
      en: 'Priority',
      no: 'Prioritet'
    },
    imageWithText: {
      en: 'Image with text',
      no: 'Bilde med tekst'
    },
    imageNoText: {
      en: 'Image without text',
      no: 'Bilde uten tekst'
    },
    imageTitle: {
      en: 'Image title',
      no: 'Bilde tittel'
    }

  }
};

export default categoriesLang;