import generalLang from "../generalSchemaLang";

// Services
const documentationLang = {
  ...generalLang,
  title: {
    en: 'Documentation',
    no: 'Dokumentasjon'
  },
  config: {
    ...generalLang,
    title: {
      en: 'Documentation config',
      no: 'Dokumentasjonskonfigurasjon'
    },
    defaultImage: {
      en: 'Default image',
      no: 'Standardbilde'
    }
  }




};

export default documentationLang;