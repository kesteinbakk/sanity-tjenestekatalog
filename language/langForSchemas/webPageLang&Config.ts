import generalLang from "../generalSchemaLang";



export const webPageLanguage = {
  ...generalLang,
  title: {
    en: 'Web page language',
    no: 'Web side språk'
  },
  sortAlpha: {
    en: 'Sort alphabetically',
    no: 'Sorter alfabetisk'
  },
  fields: {
    ...generalLang.fields,

  }
};

export const webPageConfig = {
  ...generalLang,
  title: {
    en: 'Web page config',
    no: 'Web side konfigurasjon',
  },

  fields: {
    ...generalLang.fields,
    version: {
      en: 'Version',
      no: 'Versjon',
    },
    contactMail: {
      en: 'Contact e-mail',
      no: 'Kontakt e-post'
    }
  }
};
