import generalLang from "../generalSchemaLang";

const customersLang = {
  ...generalLang,
  title: {
    en: 'Customers',
    no: 'Kunder'
  },
  fields: {
    ...generalLang.fields,
    notes: {
      en: 'Notes',
      no: 'Notater'
    },
    muni: {
      en: 'Municipality',
      no: 'Kommune'
    },
    otherPublic: {
      en: 'Other public',
      no: 'Annen offentlig'
    },
    private: {
      en: 'Private',
      no: 'Privat'
    },
    other: {
      en: 'Other',
      no: 'Andre (stiftelser etc)'
    }
  }
};

export default customersLang;