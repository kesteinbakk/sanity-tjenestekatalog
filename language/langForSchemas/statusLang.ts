import generalLang from "../generalSchemaLang";

const statusLang = {
  ...generalLang,
  title: {
    en: 'Management status',
    no: 'Forvaltningsstatus'
  },
  fields: {
    ...generalLang.fields,

    service: {
      en: 'Service',
      no: 'Tjeneste'
    },

    log: {
      en: 'Log',
      no: 'Logg'
    }
  }
};

export default statusLang;