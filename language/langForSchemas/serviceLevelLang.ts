import generalLang from "../generalSchemaLang";

const title = {
  en: 'Service Level',
  no: 'Tjenestenivå'
};
const serviceLevelLang = {
  ...generalLang,
  title,


  fields: {
    serviceLevel: {
      en: 'Service Level',
      no: 'Tjenestenivå'
    },
    ...generalLang.fields,

  }
};

export default serviceLevelLang;