import generalLang from "../../generalSchemaLang";

const localeContentLang = {
  ...generalLang,
  title: {
    en: 'Content',
    no: 'Innhold'
  },

  fields: {
    ...generalLang.fields,
    serviceLevel: {
      en: 'Service level',
      no: 'Tjenestenivå'
    },

    explanation: {
      en: 'Explanation',
      no: 'Forklaring'
    },

    notes: {
      en: 'Notes',
      no: 'Notater'
    }
  }
};

export default localeContentLang;