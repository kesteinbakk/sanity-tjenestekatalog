import generalLang from "../../generalSchemaLang";

const localeServiceContentLang = {
  ...generalLang,

  content: {
    en: 'Content',
    no: 'Innhold'
  },

  fields: {
    ...generalLang.fields,
    lead: {
      en: 'Lead paragraph',
      no: 'Ingress',
    },


  }
};

export default localeServiceContentLang;