import generalLang from "../../generalSchemaLang";

const localeStrngLang = {
  ...generalLang,
  title: {
    en: 'Localized string',
    no: 'Lokal tekst'
  },

  fields: {
    ...generalLang.fields,

  }
};

export default localeStrngLang;