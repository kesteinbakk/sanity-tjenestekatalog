import generalLang from "../generalSchemaLang";

const managedServiceInfoLang = {
  ...generalLang,
  title: {
    en: 'Management info',
    no: 'Forvaltningsinformasjon'
  },
  fields: {
    ...generalLang.fields,

    service: {
      en: 'Service',
      no: 'Tjeneste'
    },
    serviceStatus: {
      en: 'Service status',
      no: 'Tjeneste status'
    },
    ok: {
      en: 'OK',
      no: 'OK',
    },
    warning: {
      en: 'Warning',
      no: 'Advarsel'
    },
    stopped: {
      en: 'Stopped',
      no: 'Stoppet'
    },
    log: {
      en: 'Log',
      no: 'Logg'
    }
  }
};

export default managedServiceInfoLang;