import generalLang from "../generalSchemaLang";

const statusLogItemsLang = {
  ...generalLang,
  title: {
    en: 'Log item',
    no: 'Logg element'
  },


  fields: {
    ...generalLang.fields,
    date: {
      en: 'Date',
      no: 'Dato'
    },
    serviceStatus: {
      en: 'Service status',
      no: 'Tjeneste status'
    },
    success: {
      en: 'Success',
      no: 'Velykket',
    },

    warning: {
      en: 'Warning',
      no: 'Advarsel'
    },
    error: {
      en: 'Error',
      no: 'Feil'
    },
    info: {
      en: 'Information',
      no: 'Informasjon',
    },
  },
  order: {
    newestFirst: {
      en: 'Newest first',
      no: 'Nyeste først'
    },
    oldestFirst: {
      en: 'Oldest first',
      no: 'Eldste først'
    }
  }
};

export default statusLogItemsLang;