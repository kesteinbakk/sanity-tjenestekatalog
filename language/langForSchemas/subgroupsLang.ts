import generalLang from "../generalSchemaLang";

// CategoryOfService
const subgroupsLang = {
  ...generalLang,
  title: {
    en: 'Subgroups',
    no: 'Undergrupper'
  },
  fields: {
    ...generalLang.fields,

    category: {
      en: 'Category',
      no: 'Kategori'
    },
    categoryDescription: {
      en: 'This subgroup belongs to the following category:',
      no: 'Denne undergruppen tilhører følgende kategori:'
    },
  }
};

export default subgroupsLang;

