import generalLang from "../generalSchemaLang";

const customersServices = {
  ...generalLang,
  title: {
    en: 'Customers\' services',
    no: 'Kunders tjenester',
  },
  customersServices: {
    en: 'Customer\'s services',
    no: 'Kundens tjenester'
  },
  customersServicesList: {
    en: 'List of customers\' services',
    no: 'Liste over kunders tjenester'
  },
  fields: {
    ...generalLang.fields,
    customer: {
      en: 'Customer',
      no: 'Kunde'
    },



    /*     managedServicesInfo: {
          en: 'Information about managed services',
          no: 'Informasjon om forvaltede tjenester'
        }, */
    /*  log: {
       en: 'Log',
       no: 'Logg'
     }, */




  }
};

export default customersServices;