import generalLang from "../generalSchemaLang";

const platformsLang = {
  ...generalLang,
  title: {
    en: 'Platforms (subscriptions)',
    no: 'Platforms (subscriptions)',
  },
  fields: {
    ...generalLang.fields,
    logo: {
      en: 'Logo',
      no: 'Logo',
    },
    serviceProvider: {
      en: 'Service provider',
      no: 'Tjenesteleverandør',
    }
  }
};


export default platformsLang;