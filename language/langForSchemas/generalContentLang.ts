import generalLang from "../generalSchemaLang";

const generalContentLang = {
  ...generalLang,
  title: {
    en: 'General content',
    no: 'Generelt innhold'
  },
  fields: {
    ...generalLang.fields,
    notes: {
      en: 'Notes',
      no: 'Notater'
    },
  }
}

export default generalContentLang