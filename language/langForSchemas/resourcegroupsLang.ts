import generalLang from "../generalSchemaLang";

const resourcegroupsLang = {
  ...generalLang,
  title: {
    en: 'Resource groups',
    no: 'Ressursgrupper',
  },
  fields: {
    ...generalLang.fields,

  }
};


export default resourcegroupsLang;