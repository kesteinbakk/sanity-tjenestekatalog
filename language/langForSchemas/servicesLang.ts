import generalLang from "../generalSchemaLang";

// Services
const servicesLang = {
  ...generalLang,
  title: {
    en: 'Services',
    no: 'Tjenester',
  },
  servicesByCategory: {
    en: 'Services by category',
    no: 'Tjenester pr. kateogri'
  },
  metaData: {
    en: 'Meta data',
    no: 'Metadata',
  },

  serviceByCategoryDocs: {
    en: 'Services in selected category',
    no: 'Tjenester i valgt kategori'
  },


  fields: {
    ...generalLang.fields,
    tags: {
      en: 'Tags',
      no: 'Tags',
    },
    itemNumber: {
      en: 'Item number',
      no: 'Artikkelnummer'
    },
    platform: {
      en: 'Platform (subscription)',
      no: 'Platform (subscription)',
    },
    resourcegroup: {
      en: 'Resource group',
      no: 'Ressurs gruppe',
    },
    itemName: {
      en: 'Item name',
      no: 'Item name',
    },
    appName: {
      en: 'App name',
      no: 'App name',
    },

    category: {
      en: 'Category',
      no: 'Kategori'
    },

    serviceDescription: {
      en: 'Service description',
      no: 'Tjeneste beskrivelse'
    },
    delivery: {
      en: 'Delivery & reservations',
      no: 'Levering og forbehold'
    },
    serviceLevel: {
      en: 'Service level',
      no: 'Tjenestenivå'
    },
    invoicing: {
      en: 'Invoicing',
      no: 'Fakturering'
    },
    ordering: {
      en: 'Ordering',
      no: 'Bestilling'
    },

    mandatory: {
      en: 'Mandatory',
      no: 'Obligatorisk'
    },

    mainImage: {
      en: 'Main image',
      no: 'Bilde'
    },

  },

};

export default servicesLang;