import type { PortableTextBlock } from '@portabletext/types';
import { SanityReference } from '@sanity/asset-utils';

type SanityBlock = PortableTextBlock & {
  asset?: SanityReference & {
    _type: 'reference'
  }
};
export type SanityBlockContent = SanityBlock[];

/*  = {
  _key: string,
  _type: string,
  children: {
    text: string
  }[],
  markDefs: [][],
  style: string,
}[];
 */
export type SanityImageData = {
  _type: string,
  asset: {
    _ref: string,
    type: string
  },
  crop?: any,
  hotspot?: any
};

export type ServiceData = {
  id: string,
  updated: string,
  publishedAt: string,
  category: CategoryData,
  mandatory?: boolean,
  subgroup?: {
    title?: string,
    image?: SanityImageData,
  },
  content?: ServiceDataContent,
  mainImage?: SanityImageData,
  itemNumber?: string,
  itemName?: string,
};

export type ServiceDataContent = {
  title: string,
  subtitle?: string | null,
  delivery?: SanityBlockContent | null,
  invoicing?: string | null,
  ordering?: string | null,
  serviceDescription: SanityBlockContent,
  serviceLevel?: {
    serviceLevelContent: {
      title: string,
      explanation: SanityBlockContent
    } | null
  },
};

export type CategoryData = {
  id: string,
  title?: string,
  content?: SanityBlockContent,
  priority?: number,
  image?: SanityImageData,
  imageNoText?: SanityImageData,
  imageTitle?: string,
};



export type PublicWelcomeData = {
  id: string,
  name: string,
  publishedAt: string,
  content: SanityBlockContent,
  mainImage: SanityImageData,
};

export type CustomerData = {
  id: string,
  name: string,
  title: string,
  type: 'muni' | 'private' | 'otherPublic' | 'other',
  // notes: SanityBlockContent,
  // defTitle?: string,
  // defNotes?: string,
};

export type ManagementData = {
  id: string,
  customerId: string,
  managedServices: string[]
};

export type LogData = {
  date: string,
  serviceStatus: string,
  title: string,
  content: SanityBlockContent,
};

export type StatusData = {
  id: string,
  serviceTitle: string,
  log: LogData[]
};

export type HomeApiResult = {
  test?: string,
};

export type SortServicesBy = 'title' | 'category';
export type SortServArgs = {
  by?: SortServicesBy,
  ascending?: boolean,
};

export type SortCategoriesBy = 'priority' | 'title';
export type SortCatArgs = {
  by?: SortCategoriesBy,
  ascending?: boolean,
};

export type CategoryWithServicesData = CategoryData & {
  services: ServiceData[]
};

export type CategoriesById = { [key: string]: CategoryWithServicesData };
export type ServicesById = { [key: string]: ServiceData };
export type DataById = {
  services: ServicesById,
  categories: CategoriesById,
};
