type ConfigItem = {
  id: string,
  useTemplate: boolean,
  includeOrg?: boolean,
  actions?: string[]
} | string

type DeskOrder = ConfigItem[][]

export type Lang = 'no' | 'en'

export const defaultLang: Lang = 'no';

export const supportedLanguages: Lang[] = ['no', 'en'];

export const useDemoData = false;

export const storageKeys = {
  management: 'serviceManagementInputExpanded'
}

export const apiVersion = 'v2021-10-21'

export const getSortedLangs = (): Lang[] => supportedLanguages
  .sort((a, b) => {
    if (a === defaultLang) return -1
    if (b === defaultLang) return 1
    //if ( a === 'en') return -1
    return 0
  })


export const allowNewDocs = [
  'customers',
  'categories',
  'subgroups',
  'serviceLevel',
  'services',
  'customersServices',
  'posts',
  'platforms',
  'resourcegroups',
  'documentation',
  'documentationConfig',
  'generalContent',
];

export const newDocumentButtonItems = [
  'services',
  'documentation',
];

const content = [
  'generalContent',
  'documentation',
  'documentationConfig',
];

const services = [
  {
    id: 'services',
    useTemplate: true,
    includeOrg: true,
  }
];

const customersServices = [
  'customers',
  {
    id: 'customersServices',
    useTemplate: true
  },
];

const contentConfigItems = [
  'categories',
  'serviceLevel',
  'platforms',
  'resourcegroups'
];

export const deskOrder: DeskOrder = [
  services,
  customersServices,
  content,
  contentConfigItems,
];

