type TypeOfStorage = 'session' | 'local';

type Storages = {
  local: typeof window.localStorage,
  session: typeof window.sessionStorage,
};

type StrObj = { [key: string]: any };

type Api = {
  clear: () => void,
  get: (key: string) => any,
  set: (key: string, value: any) => void,
  merge: (objKey: string, obj: StrObj, val: StrObj) => StrObj,
  remove: (key: any) => void,
};

type StorageFunc = (use?: TypeOfStorage) => Api;

export const getStorage: StorageFunc = (use = 'local') => {
  // const use = 'local'

  const storages: Storages = {
    local: window.localStorage,
    session: window.sessionStorage,
  };

  const storage = storages[use]; // window.localStorage

  if (!storage) {
    throw new Error(`Invalid storage. Tried to use ${use}`);
  }

  const api: Api = {

    clear: () => {
      console.debug('Clearing local storage');
      storage.clear();
    },

    get: (key) => storage[key] && JSON.parse(storage[key]),

    set: (key, value) => storage.setItem(key, JSON.stringify(value)),

    merge: (objKey, obj) => {
      const prevVal = api.get(objKey);

      if (!prevVal || typeof prevVal !== 'object') {
        throw Error('Cannot merge without stored object. Use set first');
      } else {
        const merged = { ...prevVal, ...obj };

        api.set(objKey, merged);

        return merged;
      }
    },

    remove: (key) => storage.removeItem(key),

  };

  return api;
};



export default getStorage();
