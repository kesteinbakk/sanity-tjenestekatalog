export const truncate = (str: string, n: number, useWordBoundary?: boolean) => {
  if (str.length <= n) { return str; }
  const subString = str.substring(0, n - 1); // the original check
  return (useWordBoundary
    ? subString.substring(0, subString.lastIndexOf(" "))
    : subString) + "...";
};

export const firstUpper = (str = '') => str && str[0].toUpperCase() + str.substring(1);

export const filterDrafts = (id: string) => id.substring(0, 6) !== 'drafts';

export const sortByStringOrNumber = (
  a: string | number | undefined,
  b: string | number | undefined,
  ascending: boolean,
) => {
  // console.debug('a b', serviceTitleA, serviceTitleB);
  if (!a) {
    return ascending ? 1 : -1;
  }
  if (!b) {
    return ascending ? -1 : 1;
  }
  if (a < b) {
    return ascending ? -1 : 1;
  }
  if (a > b) {
    return ascending ? 1 : -1;
  }
  // strings or numbers must be equal
  return 0;
};


