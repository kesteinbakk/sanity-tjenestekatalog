import S from '@sanity/desk-tool/structure-builder';
//import sanityClient from 'part:@sanity/base/client'

import { getLangFunc } from '../language/lang';
import { deskOrder } from '../settings';
import customTemplates from './customTemplates';

//const client = sanityClient.withConfig({ apiVersion: '2021-06-07' })

const lang = getLangFunc('config');
//console.clear();

/* const client = sanityClient({
  projectId: '5leqs3qb',
  dataset: 'production',
  apiVersion: '2021-03-25', // use current UTC date - see "specifying API version"!
  token: '', // or leave blank for unauthenticated usage
  useCdn: false, // `false` if you want to ensure fresh data
}) */
/*
const query = '*[_type == "customers"] {_id, name}'
const params = {}

 client.fetch(query, params).then((data) => {
  //console.log('data query:', data)

}) */

const failedtoFindError = (item) => { throw Error(`${item} not found as a document type list item!`); };

const items = deskOrder.map(group => {
  //console.clear()
  //console.log('Group is', group)
  return [...group.flatMap(currentObjOrStringId => {
    // Handle input from settings.js
    const currentObj = typeof currentObjOrStringId === 'string' ?
      {
        id: currentObjOrStringId,
        useTemplate: false,
      } : currentObjOrStringId;
    const currentId = currentObj.id;
    if (!currentId || typeof currentId !== 'string') {
      console.error('Input from settings was:', currentObjOrStringId, 'Obj is', currentObj, 'Id is', currentId);
      throw Error('Missing/invalid deskOrderId');
    }

    /* Check if item exists */
    const item = S.documentTypeListItems().find(item => item.getId() === currentId)
      || failedtoFindError(currentId);

    // Check if template(s) exists if we should use any special templates
    if (currentObj.useTemplate && !customTemplates[currentId]) {
      throw Error(`Was told to use special template but could not find any templates for id "${currentId}"`);
    }

    return [
      // Should we include org item?
      ...!currentObj.useTemplate || currentObj.includeOrg ? [item] : [],
      // Should we include special template
      ...currentObj.useTemplate ? customTemplates[currentId] : []
    ];
  }),
  S.divider()
  ];
  // TODO: Implement default order, if present in fields.
  // Implement.defaultOrdering([{ field: 'position', direction: 'asc' }])
  //   S.divider()
}).flat();


//console.debug('ITEMS', items)
const deskStructure = () =>
  S.list()
    .title(lang('title'))
    .items(items);

export default deskStructure;


