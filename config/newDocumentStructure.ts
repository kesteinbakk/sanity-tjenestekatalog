import S from '@sanity/base/structure-builder'
import { newDocumentButtonItems } from '../settings'
//import client from 'part:@sanity/base/client'
//console.log(S.defaultInitialValueTemplateItems().map(item => item.getId()))
//console.log(newDocumentButtonItems)
export default [
  ...S.defaultInitialValueTemplateItems().filter(item => newDocumentButtonItems.includes(item.getId())),

]


