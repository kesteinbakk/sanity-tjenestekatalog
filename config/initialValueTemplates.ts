import T from '@sanity/base/initial-value-template-builder';
//import { MdCode } from 'react-icons/md'
import { FcServices } from 'react-icons/fc';
import { getLangFunc } from '../language/lang';
import { allowNewDocs } from '../settings';
// import { GrStatusInfo } from 'react-icons/gr';

const sLang = getLangFunc('services');
const csLang = getLangFunc('customersServices');

const initialValueTemplates = [
  ...T.defaults().filter(item => allowNewDocs.includes(item.spec.id)),

  // Service templates

  // Service by type
  T.template({
    id: 'services-by-category',
    title: sLang('servicesByCategory'),
    schemaType: 'services',
    parameters: [{ name: 'categoryId', type: 'string' }],
    icon: FcServices,
    value: params => ({
      category: { _ref: params.categoryId },
      publishedAt: (new Date()).toISOString()
    })
  }),

  // Service by user group
  /*   T.template({
      id: 'service-by-user-group',
      title: lang('serviceByUserGroup'),
      schemaType: 'services',
      parameters: [{ name: 'userGroupId', type: 'string' }],
      icon: FcServices,
      value: params => ({
        userGroup: { _ref: params.userGroupId },
        publishedAt: (new Date()).toISOString()
      })
    }),
   */



  // Management by customer
  T.template({
    id: 'customersServices-by-customer',
    title: csLang('title'),
    schemaType: 'customersServices',
    parameters: [{ name: 'id', type: 'string' }],
    icon: FcServices,
    value: params => ({
      customer: { _ref: params.id },
    })
  }),


  // Status by customer
  /*   T.template({
      id: 'status-by-service',
      title: lang('statusByService'),
      schemaType: 'status',
      parameters: [{ name: 'id', type: 'string' }],
      icon: GrStatusInfo,
      value: params => ({
        service: { _ref: params.id },
      })
    }), */

];

export default initialValueTemplates;