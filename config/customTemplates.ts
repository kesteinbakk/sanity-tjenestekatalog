import S from '@sanity/desk-tool/structure-builder';

import { getLangFunc } from '../language/lang';

const sLang = getLangFunc('services');
const csLang = getLangFunc('customersServices');

const customTemplates = {

  services: [
    S.listItem()
      .title(sLang('servicesByCategory'))
      .child(
        S.documentTypeList('categories')
          .title(sLang('servicesByCategory'))
          .child(categoryId =>
            S.documentList()
              .title(sLang('serviceByCategoryDocs'))
              .filter('_type == "services" && $categoryId == category._ref')
              .params({ categoryId })
              .initialValueTemplates([
                // @ts-ignore
                S.initialValueTemplateItem('services-by-category', { categoryId })
              ])
          )
      ),
  ],

  customersServices: [
    S.listItem()
      .title(csLang('title'))
      .child(
        S.documentList()
          .id('managementList')
          .title(csLang('customersServicesList') + ':')
          .schemaType('managementList')
          .filter('_type == "customers"')

          .child(id =>
            S.editor()
              .title(csLang('customersServices'))
              .schemaType('customersServices')
              .documentId('customersServicesFor' + id)
              .initialValueTemplate(
                'customersServices-by-customer', { id }
              )
          )
      )

  ],
  /*
    status: [
      S.listItem()
        .title(lang('status'))
        .child(
          S.documentList()
            .id('statusList')
            .title(lang('status'))
            .schemaType('statusList')
            .filter('_type == "services"')
  
            .child(id =>
              S.editor()
                .title(lang('status'))
                .schemaType('status')
                .documentId('statusFor' + id)
                .initialValueTemplate(
                  'status-by-service', { id }
                )
            )
  
        )
    ], */

  /*   webPageConfig: [
      S.listItem()
        .title(getLangFunc('webPageConfig')('title'))
        .child(
          S.editor()
            .title(getLangFunc('webPageConfig')('title'))
            .schemaType('webPageConfig')
            .documentId('webPageConfig')
        ),
    ], */
};

export default customTemplates;